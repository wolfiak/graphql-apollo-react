import React, { Component } from "react";
import ApolloClinet from "apollo-boost";
import gql from "graphql-tag";
import { ApolloProvider, ApolloConsumer } from "react-apollo";

const client = new ApolloClinet({
  uri: "http://localhost:4000/"
});

class App extends Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <ApolloConsumer>
          {client => {
            client
              .query({
                query: gql`
                  {
                    recipes {
                      id
                      title
                    }
                  }
                `
              })
              .then(result => {
                console.log("result: ", result);
              })
              .catch(err => {
                console.log("err: ", err);
              });

              return null;
          }}
        </ApolloConsumer>
        <div className="App" />
      </ApolloProvider>
    );
  }
}

export default App;
